var gulp = require("gulp"),
    watch = require("gulp-watch"),
    browserSync = require("browser-sync").create();

gulp.task("watch", function(){
    browserSync.init({
        notify: false,
        server:{
            baseDir: "app"
        }
    });
    
    watch("./app/index.html", function(){
        browserSync.reload();
    });
    watch("./app/assets/styles/**/*.css", gulp.series('css', 'cssInject'));
});

gulp.task("cssInject", function(){
   return gulp.src("./app/css/style.css")
    .pipe(browserSync.stream());
});
//This is for the versions before 3.7 and 3.7
//gulp.task("watch", function(){
//    watch("./app/index.html", gulp.series('html'));
//    watch("./app/assets/styles/**/*.css", function(){
//        gulp.start('html');
//});
